<?php

/**
 * 361GRAD Element City Teaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_cityteaser'] =
    '{type_legend},type;' .
    '{cityteaser_legend},dse_teaserimagebig,dse_teasertitle,dse_linktarget,dse_teaserimage;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_teasertitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_teasertitle'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_linktarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_linktarget'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker'
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_teaserimage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['singleSRC'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => 'jpg,jpeg,png,gif,svg',
        'tl_class'   => 'clr',
    ],
    'sql'       => 'binary(16) NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_teaserimagebig'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_teaserimagebig'],
    'inputType' => 'checkbox',
    'exclude'   => true,
    'eval'      => [
        'tl_class' => 'm12'
    ],
    'sql'       => "char(1) NOT NULL default ''"
];
