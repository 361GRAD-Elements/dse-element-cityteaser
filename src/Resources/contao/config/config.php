<?php

/**
 * 361GRAD Element City Teaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_cityteaser'] =
    'Dse\\ElementsBundle\\ElementCityteaser\\Element\\ContentDseCityteaser';

// Cascading Style Sheets
$GLOBALS['TL_CSS']['dse_cityteaser'] = 'bundles/dseelementcityteaser/css/fe_ce_dse_cityteaser.css';
