<?php

/**
 * 361GRAD Element City Teaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']   = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_cityteaser'] = ['City Teaser', 'City Teaser'];

$GLOBALS['TL_LANG']['tl_content']['cityteaser_legend'] = 'City Teaser';

$GLOBALS['TL_LANG']['tl_content']['dse_cityteaser']     = 'City Teaser';
$GLOBALS['TL_LANG']['tl_content']['dse_teaserimagebig'] = ['Big Teaser','This teaser is twice the size of a normal teaser '];
$GLOBALS['TL_LANG']['tl_content']['dse_teasertitle']    = ['City title','Title of the teaser'];
$GLOBALS['TL_LANG']['tl_content']['dse_linktarget']     = ['Link target', 'Which city page should linked?'];



