<?php

/**
 * 361GRAD Element City Teaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']   = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_cityteaser'] = ['Städte Teaser', 'Städte Teaser'];

$GLOBALS['TL_LANG']['tl_content']['cityteaser_legend'] = 'Städte Teaser';

$GLOBALS['TL_LANG']['tl_content']['dse_cityteaser']     = 'Städte Teaser';
$GLOBALS['TL_LANG']['tl_content']['dse_teaserimagebig'] = ['Großer Teaser','Dieser Teaser wird doppelt so groß wie der rest dargestellt'];
$GLOBALS['TL_LANG']['tl_content']['dse_teasertitle']    = ['Stadt Titel','Titel für den Teaser'];
$GLOBALS['TL_LANG']['tl_content']['dse_linktarget']     = ['Link Ziel', 'Auf welche Städteseite soll verlinkt werden?'];

