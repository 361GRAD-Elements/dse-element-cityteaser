361° CITY TEASER ELEMENT

For the moment we use our elements as single bundles.
Later we will use the dse-elements-bundle as metapackage
to implement all elements at once.


Installation:

! Be sure you have access to gitlab via ssh or accesstoken !


1. Edit the contao composer.json and add these lines
```
"repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.com:361grad-elements/dse-element-cityteaser.git"
        }
],
```

2. On the cli enter and install via composer
```
composer require 361grad-elements/dse-element-cityteaser
```


3. Edit file app/AppKernel.php in contao and add to the `$bundles` array
```
new Dse\ElementsBundle\ElementCityteaser\DseElementCityteaser();
```

4. Go to the install.php and update db


5. Done
